package com.devcamp_task50_20basic_java;

public class NewDevcampApp {
    public static void main(String[] args){
        System.out.println("New Decamp App");   
        NewDevcampApp.name ("Nguyen Van A", 42);
        NewDevcampApp newString = new NewDevcampApp();
        newString.name("Nguyen Van A");
    }
    // Subtask 4: Tạo 2 method mới trong class(lớp) “NewDevcampApp”
    public void name(String name) {
        System.out.println("My name is: " + name);
    }
    
    public static void name(String name, int age) {
        System.out.println("My last name is " +  name + ", my age is  " + age); 
     
    }
}
